require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe '#load_all_taxonomies' do
    context 'Taxonomies exist.' do
      let!(:categories_taxonomy) do
        create :taxonomy, name: 'Categories taxonomy'
      end
      let!(:brands_taxonomy) do
        create :taxonomy, name: 'Brands taxonomy'
      end

      let!(:root_taxon) { create :taxon, taxonomy: categories_taxonomy }

      before do
        get :show, params: { id: root_taxon.id }
      end

      # 正しいカテゴリーツリーがインスタンス変数にセットされていること
      it 'assigns correct taxonomies in @taxonomies' do
        expect(assigns(:taxonomies)).to match [categories_taxonomy, brands_taxonomy]
      end
    end

    context 'Taxonomies do NOT exist.' do
      let!(:root_taxon) { create :taxon, taxonomy: nil }

      before do
        get :show, params: { id: root_taxon.id }
      end

      # カテゴリーツリーが存在しないため、空の配列がインスタンス変数にセットされていること
      it 'assigns empty array in @taxonomies' do
        expect(assigns(:taxonomies)).to match be_empty
      end
    end
  end

  describe 'GET #show' do
    context 'The category has products.' do
      # カテゴリー及びそれに属する商品
      let!(:products) do
        [create(:product), create(:product), create(:product)]
      end
      let!(:taxon) { create :taxon, products: products }

      # カテゴリーIDを指定してアクセス
      before do
        get :show, params: { id: taxon.id }
      end

      # HTTPステータスが200であること
      it 'returns http success' do
        expect(response).to have_http_status :success
      end

      # 正しいビューテンプレートが返却されてること
      it 'returns potepan/categories/show' do
        expect(response).to render_template :show
      end

      # 正しいカテゴリーがインスタンス変数にセットされていること
      it 'assigns correct taxon in @taxon' do
        expect(assigns(:taxon)).to eq taxon
      end

      #  正しい商品一覧がインスタンス変数にセットされていること
      it 'assigns correct products in @products' do
        expect(assigns(:products)).to match products
      end
    end

    context 'The category has products and progeny products.' do
      # 親カテゴリー及びそれに属する商品
      let!(:root_products) do
        [create(:product), create(:product), create(:product)]
      end
      let!(:root_taxon) do
        create :taxon, name: 'root_taxon', products: root_products
      end

      # 子孫カテゴリー及びそれに属する商品
      let!(:progeny_products) do
        [create(:product), create(:product), create(:product)]
      end
      let!(:progeny_taxon) do
        create :taxon, name: 'progeny_taxon', products: progeny_products, parent_id: root_taxon.id
      end

      # カテゴリーIDを指定してアクセス
      before do
        get :show, params: { id: root_taxon.id }
      end

      # HTTPステータスが200であること
      it 'returns http success' do
        expect(response).to have_http_status :success
      end

      # 正しいビューテンプレートが返却されてること
      it 'returns potepan/categories/show' do
        expect(response).to render_template :show
      end

      # 正しいカテゴリーがインスタンス変数にセットされていること
      it 'assigns correct taxon in @taxon' do
        expect(assigns(:taxon)).to eq root_taxon
      end

      #  親カテゴリーに属する商品と子孫カテゴリーに属する商品の一覧がインスタンス変数にセットされていること
      it 'assigns correct root products and progeny products in @products' do
        expect(assigns(:products)).to match root_products.concat(progeny_products)
      end
    end

    context 'The category has unavailable products.' do
      # カテゴリー及びそれに属する無効な商品（削除済みの商品、有効開始日時が明日の商品）
      let!(:unavailable_products) do
        [
          create(:product, deleted_at: Time.now),
          create(:product, available_on: Time.now.since(1.days)),
        ]
      end
      let!(:taxon) { create :taxon, products: unavailable_products }

      # カテゴリーIDを指定してアクセス
      before do
        get :show, params: { id: taxon.id }
      end

      # HTTPステータスが200であること
      it 'returns http success' do
        expect(response).to have_http_status :success
      end

      # 正しいビューテンプレートが返却されてること
      it 'returns potepan/categories/show' do
        expect(response).to render_template :show
      end

      # 正しいカテゴリーがインスタンス変数にセットされていること
      it 'assigns correct taxon in @taxon' do
        expect(assigns(:taxon)).to eq taxon
      end

      # 有効な商品が所属していないため、商品一覧のインスタンス変数には空の配列がセットされていること
      it 'assigns empty array in @products' do
        expect(assigns(:products)).to match be_empty
      end
    end

    context 'The category does NOT have any available products.' do
      # 商品が紐づいていないカテゴリー
      let!(:taxon) { create :taxon }

      # カテゴリーIDを指定してアクセス
      before do
        get :show, params: { id: taxon.id }
      end

      # HTTPステータスが200であること
      it 'returns http success' do
        expect(response).to have_http_status :success
      end

      # 正しいビューテンプレートが返却されてること
      it 'returns potepan/categories/show' do
        expect(response).to render_template :show
      end

      # 正しいカテゴリーがインスタンス変数にセットされていること
      it 'assigns correct taxon in @taxon' do
        expect(assigns(:taxon)).to eq taxon
      end

      # 商品が所属していないため、商品一覧のインスタンス変数には空の配列がセットされていること
      it 'assigns empty array in @products' do
        expect(assigns(:products)).to match be_empty
      end
    end
  end
end
