require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    # メイン商品
    let!(:t_shirt) { create :product, slug: 't-shirt' }

    # 同一カテゴリ(taxon)に属している有効な関連商品
    let!(:dress_shirt) { create :product, available_on: Time.now.ago(1.hour) }
    let!(:polo_shirt)  { create :product, available_on: Time.now.ago(2.hour) }
    let!(:tote_bag)    { create :product, available_on: Time.now.ago(3.hour) }
    let!(:hand_bag)    { create :product, available_on: Time.now.ago(4.hour) }
    let!(:mug_cup)     { create :product, available_on: Time.now.ago(5.hour) }

    # 同一カテゴリ(taxon)に属している無効な関連商品
    let!(:deleted)     { create :product, deleted_at:   Time.now }
    let!(:unavailable) { create :product, available_on: Time.now.since(1.days) }

    # 別のカテゴリ(other_taxon)に属している商品（関連商品ではない）
    let!(:other_taxon_product) { create :product, slug: 'other-taxon-product' }

    let!(:taxonomy) { create :taxonomy }
    let!(:taxon) do
      create(
        :taxon,
        taxonomy: taxonomy,
        products: [
          t_shirt,
          deleted,
          mug_cup,
          hand_bag,
          tote_bag,
          polo_shirt,
          dress_shirt,
          unavailable,
        ]
      )
    end
    let!(:other_taxon) do
      create(
        :taxon,
        taxonomy: taxonomy,
        products: [
          other_taxon_product,
        ]
      )
    end

    # URLで指定された商品が存在し、関連商品がある場合
    context 'The available product exists with related products.' do
      # 商品詳細ページにアクセス
      before do
        get :show, params: { id: t_shirt.slug }
      end

      # HTTPステータスが200であること
      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end

      # 正しいビューテンプレートが返却されてること
      it 'returns potepan/products/show' do
        expect(response).to render_template :show
      end

      # 正しいインスタンス変数がセットされていること
      it 'assigns correct product in @product' do
        expect(assigns(:product)).to eq t_shirt
      end

      # 有効になった日付の降順で4つの関連商品がリストにセットされていること
      it 'assigns correct array products in @related_products' do
        expected_products = [dress_shirt, polo_shirt, tote_bag, hand_bag]

        expect(assigns(:related_products)).to match expected_products
      end
    end

    # URLで指定された商品が存在し、関連商品がない場合
    context 'The available product exists WITHOUT related products.' do
      before do
        get :show, params: { id: other_taxon_product.slug }
      end

      # HTTPステータスが200であること
      it 'returns http success' do
        expect(response).to have_http_status(:success)
      end

      # 正しいビューテンプレートが返却されてること
      it 'returns potepan/products/show' do
        expect(response).to render_template :show
      end

      # 正しいインスタンス変数がセットされていること
      it 'assigns correct product in @product' do
        expect(assigns(:product)).to eq other_taxon_product
      end

      # 同一カテゴリーに属する商品がないため、空の配列がリストにセットされていること
      it 'assigns empty array in @related_products' do
        expect(assigns(:related_products)).to match be_empty
      end
    end
  end
end
