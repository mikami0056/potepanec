require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  # カテゴリーツリー
  given! :taxonomy_of_caetories do
    create :taxonomy, name: 'Categories'
  end
  given! :taxonomy_of_brands do
    create :taxonomy, name: 'Brands'
  end

  # Categoriesツリー配下の各商品
  given!(:jersey)     { create :product, name: 'Jersey' }
  given!(:sweatshirt) { create :product, name: 'Sweatshirt' }
  given!(:t_shirt)    { create :product, name: 'T-shirt' }

  # Categoriesツリーのルートカテゴリー
  given! :clothes_taxon do
    create(
      :taxon,
      name: 'Clothes',
      taxonomy: taxonomy_of_caetories,
      products: [jersey, sweatshirt]
    )
  end
  # 子孫カテゴリー
  given! :t_shirts_taxon do
    create(
      :taxon,
      name: 'T-Shirts',
      parent_id: clothes_taxon.id,
      taxonomy: taxonomy_of_caetories,
      products: [t_shirt]
    )
  end

  # Brandsツリー配下の各商品
  given!(:takeo_wallet) { create :product, name: 'Takeo Wallet' }
  given!(:tk_shoes)     { create :product, name: 'TK Shoes' }

  # Brandsツリーのルートカテゴリー
  given! :takeo_taxon do
    create(
      :taxon,
      name: 'TAKEO KIKUCHI',
      taxonomy: taxonomy_of_brands,
      products: [takeo_wallet]
    )
  end
  # 子孫カテゴリー
  given! :tk_taxon do
    create(
      :taxon,
      name: 'TK',
      parent_id: takeo_taxon.id,
      taxonomy: taxonomy_of_brands,
      products: [tk_shoes]
    )
  end

  before do
    visit potepan_category_path(clothes_taxon.id)
  end

  scenario 'displaying correct URL, Page Title, and Page Header' do
    # URL
    expect(page).to have_current_path("/potepan/categories/#{clothes_taxon.id}")

    # ページタイトル
    expect(page).to have_title("#{clothes_taxon.name} - BIGBAG Store")

    # ページヘッダータイトル
    expect(page).to have_selector 'h2', text: clothes_taxon.name

    # パンくず
    expect(page).to have_selector 'li', text: clothes_taxon.name
  end

  scenario 'displaying side bar' do
    # カテゴリーツリー名、ブランドツリー名が表示されていること
    within('.side-nav') do
      expect(page).to have_selector 'li', text: taxonomy_of_caetories.name
      expect(page).to have_selector 'li', text: taxonomy_of_brands.name
    end
  end

  scenario 'displaying products belong to clothes_taxon' do
    # clothes_taxonカテゴリーに紐づく商品が表示されていること
    expect(page).to have_content jersey.name
    expect(page).to have_content sweatshirt.name
    expect(page).to have_content t_shirt.name

    # clothes_taxonカテゴリーに紐づいていない商品が表示されていないこと
    expect(page).to have_no_content takeo_wallet.name
    expect(page).to have_no_content tk_shoes.name
  end
end
