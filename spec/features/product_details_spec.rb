require 'rails_helper'

RSpec.feature 'ProductDetails', type: :feature do
  # 商品詳細ページが正しく表示された場合
  context 'The product detail page is loaded successfully.' do
    given!(:product) { create :product, name: 'Test product', slug: 'test-product' }

    # 「一覧ページへ戻る」リンクが生成される際にカテゴリー（taxon）が必要なため
    given!(:taxon) { create :taxon, products: [product] }

    # URL, ページタイトル、ページヘッダー（タイトル、パンくず）、「一覧ページへ戻る」リンクが正しく表示されていること
    scenario 'displaying correct URL, Page Title, Page Header, and Link for going to list' do
      visit potepan_product_path product

      # URL
      expect(page).to have_current_path "/potepan/products/#{product.slug}"

      # ページタイトル
      expect(page).to have_title "#{product.name} - BIGBAG Store"

      # ページヘッダー
      within '.lightSection' do
        # タイトル
        expect(page).to have_selector 'h2', text: product.name

        # パンくず
        expect(page).to have_selector 'li', text: product.name
      end

      # 一覧ページへ戻る
      expect(page).to have_link '一覧ページへ戻る', href: "/potepan/categories/#{taxon.id}"
    end
  end

  # 対象の商品は存在し、色、サイズのバリエーションがある場合
  context 'The product exists and has variants of colors or sizes.' do
    given!(:product_with_variants) { create(:variant).product }
    given!(:taxon) { create :taxon, products: [product_with_variants] }

    # 商品詳細情報、色とサイズ、数量、及びカート追加フォームが表示される
    scenario 'The page displays products details, colors, sizes, amount, and cart form' do
      visit potepan_product_path product_with_variants

      # 商品情報
      within '.media-body' do
        # 商品名
        expect(page).to have_content product_with_variants.name

        # 価格
        expect(page).to have_content product_with_variants.display_price

        # 概要
        expect(page).to have_content product_with_variants.description
      end

      # カート追加フォーム
      within '#cart-form' do
        # サイズと色を選択するプルダウンが表示されている
        expect(page).to have_selector 'select#cart_form_variant'

        # 購入数を選択するプルダウン
        expect(page).to have_selector 'select#cart_form_amount'

        # カートへ追加するボタン
        expect(page).to have_selector 'button.btn-primary', text: 'カートへ入れる'
      end
    end
  end

  # 対象の商品は存在するが、色、サイズのバリエーションがない場合
  context 'The product exists, but it does NOT have variants of colors or sizes.' do
    given!(:product_without_variants) { create :product }
    given!(:taxon) { create :taxon, products: [product_without_variants] }

    # 商品詳細情報、数量、及びカート追加フォームが表示される
    scenario 'The page displays products details, amount, and cart form' do
      visit potepan_product_path product_without_variants

      # 商品情報
      within '.media-body' do
        # 商品名
        expect(page).to have_content product_without_variants.name

        # 価格
        expect(page).to have_content product_without_variants.display_price

        # 概要
        expect(page).to have_content product_without_variants.description
      end

      # カート追加フォーム
      within '#cart-form' do
        # サイズと色を選択するプルダウンが表示されていないこと
        expect(page).to have_no_selector 'select#cart_form_variant'

        # 購入数を選択するプルダウン
        expect(page).to have_selector 'select#cart_form_amount'

        # カートへ追加するボタン
        expect(page).to have_selector 'button.btn-primary', text: 'カートへ入れる'
      end
    end
  end

  # 対象の商品に関連商品が存在する場合
  context 'The product has related products.' do
    # 対象商品
    given!(:t_shirt) { create :product, name: 'T shirt', price: 12.00 }

    # 関連商品
    given!(:sweat_shirt) { create :product, name: 'Sweat shirt', price: 15.00 }
    given!(:polo_shirt)  { create :product, name: 'Polo shirt',  price: 20.00 }
    given!(:tote_bag)    { create :product, name: 'Tote bag',    price: 10.00 }
    given!(:back_pack)   { create :product, name: 'Back pack',   price: 30.00 }

    given!(:taxon) do
      create(
        :taxon,
        products: [
          t_shirt,
          sweat_shirt,
          polo_shirt,
          tote_bag,
          back_pack,
        ]
      )
    end

    # 関連商品の名前、値段が表示されていること
    scenario 'The page displays related products names and prices' do
      visit potepan_product_path t_shirt

      within '.productsContent' do
        expect(page).to have_content sweat_shirt.name
        expect(page).to have_content sweat_shirt.display_price

        expect(page).to have_content polo_shirt.name
        expect(page).to have_content polo_shirt.display_price

        expect(page).to have_content tote_bag.name
        expect(page).to have_content tote_bag.display_price

        expect(page).to have_content back_pack.name
        expect(page).to have_content back_pack.display_price
      end
    end
  end
end
