require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper

  # 完全ページ生成メソッド
  describe "#generate_full_title" do
    # 共通タイトル
    let(:base_title) { "BIGBAG Store" }

    # 共通タイトルのみが返却される場合
    context "when base title is only returned" do
      # 引数がないため
      it "the argument does not exist" do
        expect(generate_full_title).to(eq(base_title))
      end

      # 引数がnilのため
      it "the argument is nil" do
        expect(generate_full_title(page_title: nil)).to(eq(base_title))
      end

      # 引数が空文字のため
      it "the argument is empty" do
        expect(generate_full_title(page_title: "")).to(eq(base_title))
      end

      # 引数が半角空白のみのため
      it "the argument is only consisted of whitespaces" do
        expect(generate_full_title(page_title: "   ")).to(eq(base_title))
      end

      # 引数が全角空白のみのため
      it "the argument is only consisted of 2byte whitespaces" do
        expect(generate_full_title(page_title: "　　　")).to(eq(base_title))
      end

      # 引数が半角空白、全角空白の混合体のため
      it "the argument is only consisted of whitespaces and 2byte whitespaces" do
        expect(generate_full_title(page_title: " 　 　 　")).to(eq(base_title))
      end
    end

    # 完全タイトルが返却される場合
    context "when full title is returned" do
      # 正しい引数のため
      it "the argument is valid" do
        expect(generate_full_title(page_title: "test title")).to(eq("test title - #{base_title}"))
      end

      # 引数は数字のみで構成されている
      it "the argument is valid, it is only consisted of numbers" do
        expect(generate_full_title(page_title: 12345)).to(eq("12345 - #{base_title}"))
      end

      # 引数の前後に半角空白あり
      it "the argument is valid, it has whitespaces with its top and end" do
        expect(generate_full_title(page_title: " test title ")).to(eq("test title - #{base_title}"))
      end

      # 引数の前後に全角空白あり
      it "the argument is valid, it has 2byte whitespaces with its top and end" do
        expect(generate_full_title(page_title: "　test title　")).to(eq("test title - #{base_title}"))
      end
    end
  end
end
