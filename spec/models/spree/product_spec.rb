require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  # 関連商品取得メソッド用テストコード
  describe '#related_products.' do
    # 商品がどのカテゴリー(taxon)にも属していない場合
    context 'the product does NOT belong to any taxons.' do
      let!(:product_no_taxons) { create :product }

      # 空の配列が返却される
      it 'returns empty array' do
        expect(product_no_taxons.related_products).to be_empty
      end
    end

    # 同一カテゴリーに他の商品が存在しない場合
    context 'there is no products that belong to same taxon.' do
      let!(:taxon)   { create :taxon }
      let!(:product) { create :product, taxons: [taxon] }

      # 空の配列が返却される
      it 'returns empty array' do
        expect(product.related_products).to be_empty
      end
    end

    # 同一カテゴリーに属する商品が全て利用不可（削除済み、有効期間外）の場合
    context 'there is all unavailable products that belong to same taxon.' do
      let!(:deleted_product)     { create :product, deleted_at: Time.now }
      let!(:unavailable_product) { create :product, available_on: Time.now.since(1.days) }
      let!(:taxon) { create :taxon, products: [deleted_product, unavailable_product] }

      let!(:product) { create :product, taxons: [taxon] }

      # 空の配列が返却される
      it 'returns empty array' do
        expect(product.related_products).to be_empty
      end
    end

    # 同一カテゴリーに属する商品が6つある場合
    context 'there is six available products that belong to same taxon.' do
      # product_X (X:有効開始日時が若い順)
      let!(:product_1) { create :product, available_on: Time.now.ago(1.hour) }
      let!(:product_3) { create :product, available_on: Time.now.ago(3.hour) }
      let!(:product_5) { create :product, available_on: Time.now.ago(5.hour) }
      let!(:taxon_odd) { create :taxon, products: [product_1, product_3, product_5] }

      let!(:product_2)  { create :product, available_on: Time.now.ago(2.hour) }
      let!(:product_4)  { create :product, available_on: Time.now.ago(4.hour) }
      let!(:product_6)  { create :product, available_on: Time.now.ago(6.hour) }
      let!(:taxon_even) { create :taxon, products: [product_2, product_4, product_6] }

      let!(:product) { create :product, taxons: [taxon_odd, taxon_even] }

      # メソッドに引数を指定していない場合（取得件数がデフォルトの4件）
      context 'no set fetch_count' do
        # 有効開始日時が若い順で関連商品を4件取得する
        it 'return four products as array' do
          expected_products = [product_1, product_2, product_3, product_4]

          expect(product.related_products).to match expected_products
        end
      end

      # メソッド引数が5の場合
      context 'fetch_count is five' do
        # 有効開始日時が若い順で関連商品を5件取得する
        it 'return five products as array' do
          expected_products = [product_1, product_2, product_3, product_4, product_5]

          expect(product.related_products(fetch_count: 5)).to match expected_products
        end
      end

      # メソッド引数が0の場合
      context 'fetch_count is zero' do
        # 空の配列が返却される
        it 'return empty array' do
          expect(product.related_products(fetch_count: 0)).to be_empty
        end
      end
    end

    # 同じ商品がいくつかの別カテゴリーに属している場合
    context 'same products belong to some taxons.' do
      let!(:product_1) { create :product, available_on: Time.now.ago(1.hour) }
      let!(:product_2) { create :product, available_on: Time.now.ago(2.hour) }
      let!(:product_3) { create :product, available_on: Time.now.ago(3.hour) }
      let!(:product_4) { create :product, available_on: Time.now.ago(4.hour) }

      let!(:taxon_alpha)   { create :taxon, products: [product_1, product_2, product_3] }
      let!(:taxon_bravo)   { create :taxon, products: [product_1, product_4] }
      let!(:taxon_charlie) { create :taxon, products: [product_2] }

      let!(:product) { create :product, taxons: [taxon_alpha, taxon_bravo, taxon_charlie] }

      # 重複のない関連商品を4件取得する
      it 'return four products as array, not duplicate' do
        expected_products = [product_1, product_2, product_3, product_4]

        expect(product.related_products).to match expected_products
      end
    end
  end
end
