# modelを拡張する際に参考となるURL
# [https://guides.spreecommerce.org/developer/customization/logic.html]
module Potepan
  module ProductDecorator
    # 同じカテゴリー(taxons)に属している商品を、関連商品として取得するメソッド
    # 有効になった日時が新しい順で取得し、自身は含めない
    # 商品がカテゴリーに属していない、または取得件数の指定が[0]の場合は、空の配列を返却する
    #
    # @param  [Integer] fetch_count 取得件数（デフォルトは4件）
    # @return [Array<Spree::Product>] 関連商品
    def related_products(fetch_count: 4)
      # カテゴリーに属していない場合の商品全件検索を避けるため
      # [参考] solidus_core-2.7.0/app/models/spree/product/scopes.rb #in_taxons(L.75〜78)
      if taxons.blank? || fetch_count == 0
        return []
      end

      Spree::Product.
        in_taxons(taxons).
        available.
        where.not(id: id).
        includes(master: [:images, :default_price]).
        distinct.
        order(available_on: :desc).
        limit(fetch_count).
        to_a
    end
  end
end
Spree::Product.prepend Potepan::ProductDecorator
