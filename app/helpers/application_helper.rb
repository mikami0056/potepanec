module ApplicationHelper
  # 各ページの完全なタイトルを生成、返却するメソッド
  # 引数の前後に半角空白、全角空白が含まれている場合は、トリミングして完全なタイトルを生成する
  # 引数が以下の場合は、共通タイトルのみを返却する
  #   - nil
  #   - 空文字
  #   - 半角空白のみ
  #   - 全角空白のみ
  #   - 半角空白、全角空白混合
  #
  # @param  page_title [String] 各ページのタイトル
  # @return [String] 完全タイトル（各ページタイトル名 - 共通タイトル）
  # @return [String] 共通タイトル（引数が指定されていいない場合など）
  def generate_full_title(page_title: "")
    base_title = "BIGBAG Store"

    # 引数前後の半角空白、全角空白を空文字に置き換える
    trimmed_page_title = page_title.to_s.gsub(/(\A[[:space:]]+)|([[:space:]]+\Z)/, '')

    trimmed_page_title.empty? ? base_title : "#{trimmed_page_title} - #{base_title}"
  end

  # 該当のページが商品に関するページか判定するメソッド
  #
  # @return [Boolean] True  商品に関するページ
  # @return [Boolean] False それ以外のページ
  def product_pages?
    ['potepan/products', 'potepan/categories'].include?(controller_path)
  end
end
