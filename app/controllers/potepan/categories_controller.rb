class Potepan::CategoriesController < ApplicationController
  before_action :load_all_taxonomies, only: :show

  # 指定されたカテゴリーに所属する商品一覧を表示するアクション
  def show
    # 対象のレコードが存在しない場合は、ActiveRecord::RecordNotFoundが発生する
    @taxon = Spree::Taxon.includes(:products).find(params[:id])

    # 事前に画像と値段を読み込んでおく
    @products = @taxon.all_products.available.includes(master: [:images, :default_price])
  end

  private

  # 各カテゴリーが所属しているツリーを全件取得するメソッド
  # ツリーのルート分類及び、ルート分類の子分類を事前に読み込む
  #
  # @return [Array<Spree::Taxonomy>] 各分類が所属しているツリー一覧
  def load_all_taxonomies
    @taxonomies = Spree::Taxonomy.all.includes root: :children
  end
end
