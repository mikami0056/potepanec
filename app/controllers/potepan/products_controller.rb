class Potepan::ProductsController < ApplicationController
  # TODO 今後定数が増えた場合は、定数ファイルを作成するか、configやsettingslogicといったgemの使用を検討すること
  # config        [https://github.com/railsconfig/config]
  # settingslogic [https://github.com/binarylogic/settingslogic]
  # 関連商品表示件数
  RELATED_PRODUCTS_DISPLAY_COUNT = 4

  # 商品詳細表示ページ
  def show
    # 該当するレコードが存在しない場合、public/404.htmlを表示する
    @product = Spree::Product.available.friendly.find params[:id]

    # 同じカテゴリーに属している関連商品を取得
    @related_products = @product.related_products fetch_count: RELATED_PRODUCTS_DISPLAY_COUNT
  end
end
